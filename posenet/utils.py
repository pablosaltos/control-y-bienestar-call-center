import cv2
import numpy as np

import posenet.constants


def valid_resolution(width, height, output_stride=16):
    target_width = (int(width) // output_stride) * output_stride + 1
    target_height = (int(height) // output_stride) * output_stride + 1
    return target_width, target_height


def _process_input(source_img, scale_factor=1.0, output_stride=16):
    target_width, target_height = valid_resolution(
        source_img.shape[1] * scale_factor, source_img.shape[0] * scale_factor, output_stride=output_stride)
    scale = np.array([source_img.shape[0] / target_height, source_img.shape[1] / target_width])

    input_img = cv2.resize(source_img, (target_width, target_height), interpolation=cv2.INTER_LINEAR)
    input_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB).astype(np.float32)
    input_img = input_img * (2.0 / 255.0) - 1.0
    input_img = input_img.reshape(1, target_height, target_width, 3)
    return input_img, source_img, scale


def read_cap(cap, scale_factor=1.0, output_stride=16):
    res, img = cap.read()
    if not res:
        raise IOError("webcam failure")
    return _process_input(img, scale_factor, output_stride)


def read_imgfile(path, scale_factor=1.0, output_stride=16):
    img = cv2.imread(path)
    return _process_input(img, scale_factor, output_stride)


def draw_keypoints(
        img, instance_scores, keypoint_scores, keypoint_coords,
        min_pose_confidence=0.5, min_part_confidence=0.5):
    cv_keypoints = []
    for ii, score in enumerate(instance_scores):
        if score < min_pose_confidence:
            continue
        for ks, kc in zip(keypoint_scores[ii, :], keypoint_coords[ii, :, :]):
            if ks < min_part_confidence:
                continue
            cv_keypoints.append(cv2.KeyPoint(kc[1], kc[0], 10. * ks))
    out_img = cv2.drawKeypoints(img, cv_keypoints, outImage=np.array([]))
    return out_img


def get_adjacent_keypoints(keypoint_scores, keypoint_coords, min_confidence=0.1):
    results = []
    for left, right in posenet.CONNECTED_PART_INDICES:
        if keypoint_scores[left] < min_confidence or keypoint_scores[right] < min_confidence:
            continue
        results.append(
            np.array([keypoint_coords[left][::-1], keypoint_coords[right][::-1]]).astype(np.int32),
        )
    return results


def draw_skeleton(
        img, instance_scores, keypoint_scores, keypoint_coords,
        min_pose_confidence=0.5, min_part_confidence=0.5):
    out_img = img
    adjacent_keypoints = []
    for ii, score in enumerate(instance_scores):
        if score < min_pose_confidence:
            continue
        new_keypoints = get_adjacent_keypoints(
            keypoint_scores[ii, :], keypoint_coords[ii, :, :], min_part_confidence)
        adjacent_keypoints.extend(new_keypoints)
    out_img = cv2.polylines(out_img, adjacent_keypoints, isClosed=False, color=(255, 255, 0))
    return out_img


def draw_skel_and_kp(
        img, instance_scores, keypoint_scores, keypoint_coords,
        min_pose_score=0.5, min_part_score=0.5):
    out_img = img
    adjacent_keypoints = []
    cv_keypoints = []
    for ii, score in enumerate(instance_scores):
        if score < min_pose_score:
            continue

        new_keypoints = get_adjacent_keypoints(
            keypoint_scores[ii, :], keypoint_coords[ii, :, :], min_part_score)
        adjacent_keypoints.extend(new_keypoints)

        kop = (0,0) #donde dibujo el texto
        koq = (0,0)
        j=0
        cadera=0
        rodilla=0
        dif=0
        
        hombro1=0 
        muneca1=0
        hombro2=0 
        muneca2=0
        alza1=0
        alza2=0
        
        for ks, kc in zip(keypoint_scores[ii, :], keypoint_coords[ii, :, :]):
            if ks < min_part_score:
                continue
            cv_keypoints.append(cv2.KeyPoint(kc[1], kc[0], 10. * ks))
            if j==5:
                hombro1 = int(kc[0])
            if j==9:
                muneca1 = int(kc[0])
            if j==6:
                hombro2 = int(kc[0])
            if j==10:
                muneca2 = int(kc[0])
                
            if j==11:
                cadera = int(kc[0])
            if j==13:
                if int(kc[0])>0:
                    rodilla = int(kc[0])
            if j==5:
                kop=(int(kc[1]) + 5, int(kc[0]))
                koq=(int(kc[1]) + 20, int(kc[0]) + 20)
            
            j += 1
        
        #levanta la mano
        '''
        alza1 = hombro1 - muneca1
        alza2 = hombro2 - muneca2
        
        etiqueta = ""
        color_letra = (0,0,0)
        color_fondo = (255,255,255)
        if alza1>0 or alza2>0:
            etiqueta="Alza"
            color_letra = (0,0,0)
            color_fondo = (47,255,173)
        '''
        
        dif = rodilla - cadera
        etiqueta = "Parado"
        color_letra = (255,255,255)
        color_fondo = (0,0,255)
        if dif<20:
            etiqueta="Sentado"
            color_letra = (0,0,0)
            color_fondo = (47,255,173)
        
        #dibujo un texto con el numero de persona y el score
        font = cv2.FONT_HERSHEY_SIMPLEX
        texto = "#" + str(ii) + "(" + str(round(score * 100, 3)) +"%)"
        
        (text_width, text_height) = cv2.getTextSize(texto, font, fontScale=0.5, thickness=1)[0]
        text_offset_x = kop[0]
        text_offset_y = kop[1] + 8
        rectangle_bgr = (255, 255, 255)

        box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 8, text_offset_y - text_height - 10))
        cv2.rectangle(out_img, box_coords[0], box_coords[1], rectangle_bgr, cv2.FILLED)
        
        out_img=cv2.putText(out_img, texto, kop, font, 0.5, (0, 0, 0), 1)
        
        (text_width, text_height) = cv2.getTextSize(etiqueta, font, fontScale=0.5, thickness=1)[0]
        text_offset_x = koq[0]
        text_offset_y = koq[1] + 8
        rectangle_bgr = color_fondo

        box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 8, text_offset_y - text_height - 10))
        cv2.rectangle(out_img, box_coords[0], box_coords[1], rectangle_bgr, cv2.FILLED)
        
        out_img = cv2.putText(out_img, etiqueta, koq, font, 0.5, color_letra, 1, cv2.LINE_AA)
       # out_img = cv2.putText(out_img, texto, kop, font, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
        #out_img = cv2.putText(out_img, etiqueta, koq, font, 0.5, (0, 255, 255), 1, cv2.LINE_AA)
        
    out_img = cv2.drawKeypoints(
        out_img, cv_keypoints, outImage=np.array([]), color=(0, 255, 0),
        flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    out_img = cv2.polylines(out_img, adjacent_keypoints, isClosed=False, color=(255, 255, 0))
    
    return out_img
