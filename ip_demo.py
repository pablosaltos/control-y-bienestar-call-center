import tensorflow as tf
import cv2
import time
import argparse
import posenet
from pymongo import MongoClient
import urllib
from urllib.parse import unquote
import json
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=int, default=101)
parser.add_argument('--cam_id', type=int, default=0, help="El id de la webcam a utilizar")
parser.add_argument('--cam_width', type=int, default=1280)
parser.add_argument('--cam_height', type=int, default=720)
parser.add_argument('--scale_factor', type=float, default=0.5)
parser.add_argument('--file', type=str, default=None, help="Realizar la deteccion en un archivo de video")
parser.add_argument('--camera', type=int, default=1, help="Que camara usar, 1 y 2 camaras ip, 3 camara web")
parser.add_argument('--num_poses', type=int, default=10, help="Maximo numero de personas a detectar")
parser.add_argument('--save_each', type=int, default=60, help="Cada cuantos frames guardar en mongo las personas encontradas")
parser.add_argument('--save_image', type=int, default=0, help="Si se guarda una captura del video cada vez que se guardan los datos")
parser.add_argument('--show_skeleton', type=int, default=1, help="Si se dibuja el esqueleto de las personas encontradas")
parser.add_argument('--min_score', type=float, default=0.15, help="Minimo score para decir que es una persona")
parser.add_argument('--porcentaje_video', type=int, default=100, help="Redimensionar el video al ? porciento del original")
parser.add_argument('--save_video', type=int, default=0, help="Guarda un video al finalizar")
parser.add_argument('--video_name', type=str, default="out.mp4", help="Nombre del video a guardar")
parser.add_argument('--save_data', type=int, default=0, help="Si se guarda los datos en mongo")
args = parser.parse_args()

'''
modelos
50, 75, 100, 101
'''

'''
resoluciones camara ip

1MP/720p  1280 x 720
1.3MP  1280 x 1024
2MP/1080p  1920 x 1080
3MP  2048 x 1536
4MP  2688 x 1520
5MP  2944 x 1656
6MP  3072 x 2048
8MP (4K)	 3840 x 2160
12MP  4000 x 3072
'''

def main():
    print("======================================================")
    print("Presiones q para finalizar")
    print("======================================================")
    #datos de donde guardar la data
    uri = 'mongodb://admin:' + urllib.parse.quote_plus('eecgkTxZLSK7QcwK') + '@192.168.86.29:27017/admin?authMechanism=SCRAM-SHA-1'
    myclient = MongoClient(uri)
    mydb = myclient["bienestar_call_center"]
    mycol = mydb["data_bienestar_cc"]
        
    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    writer = None
    (h, w) = (None, None)
    zeros = None
    
    img_counter = 1
    #inicia una nueva sesion de tf
    with tf.Session() as sess:
        #carga el modelo a usar
        model_cfg, model_outputs = posenet.load_model(args.model, sess)
        output_stride = model_cfg['output_stride']

        if args.file is not None:
            #lee un archivo
            cap = cv2.VideoCapture(args.file)
        else:
            if args.camera==1:
                #camara ip
                cap = cv2.VideoCapture("rtsp://admin:xxx@127.0.0.1")
            elif args.camera==2:
                #camara ip
                cap = cv2.VideoCapture("rtsp://admin:xxx@127.0.0.1")
            else:
                #webcam
                cap = cv2.VideoCapture(args.cam_id)
            
        #cap.set(cv2.CAP_PROP_FRAME_WIDTH, args.cam_width)
        #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, args.cam_height)

        start = time.time()
        frame_count = 0
        frames_passed = 0
        while True:            
            #loop infinito hasta que se termina el programa o presiona la tecla q
            input_image, display_image, output_scale = posenet.read_cap(
                cap, scale_factor=args.scale_factor, output_stride=output_stride)
            
            #ajusta el brillo
            '''
            hsv = cv2.cvtColor(display_image, cv2.COLOR_BGR2HSV)
            h, s, v = cv2.split(hsv)
            brillo = 40
            lim = 255 - brillo
            v[v > lim] = 255
            v[v <= lim] += brillo
            
            final_hsv = cv2.merge((h, s, v))
            display_image = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
            '''
                        
            heatmaps_result, offsets_result, displacement_fwd_result, displacement_bwd_result = sess.run(
                model_outputs,
                feed_dict={'image:0': input_image}
            )
            
            min_pose_score = args.min_score

            pose_scores, keypoint_scores, keypoint_coords = posenet.decode_multi.decode_multiple_poses(
                heatmaps_result.squeeze(axis=0),
                offsets_result.squeeze(axis=0),
                displacement_fwd_result.squeeze(axis=0),
                displacement_bwd_result.squeeze(axis=0),
                output_stride=output_stride,
                max_pose_detections=args.num_poses,
                min_pose_score=min_pose_score)

            keypoint_coords *= output_scale
            
            overlay_image = posenet.draw_skel_and_kp(
                    display_image, pose_scores, keypoint_scores, keypoint_coords,
                    min_pose_score=min_pose_score, min_part_score=0.1)

            scale_percent = args.porcentaje_video #porcentaje del original
            width = int(overlay_image.shape[1] * scale_percent / 100)
            height = int(overlay_image.shape[0] * scale_percent / 100)
            dim = (width, height)
                
            if args.save_video==1:
                frame = overlay_image
                # check if the writer is None
                if writer is None:
                    # store the image dimensions, initialize the video writer,
                    # and construct the zeros array
                    (h, w) = frame.shape[:2]
                    #dibuja tambien videos en BRG
                    #writer = cv2.VideoWriter("out.avi", fourcc, 6.0,
                    #(w * 2, h * 2), True)
                    writer = cv2.VideoWriter(args.video_name, fourcc, 20.0,(w, h), True)
                    zeros = np.zeros((h, w), dtype="uint8")
                    
                # break the image into its RGB components, then construct the
                # RGB representation of each frame individually
                (B, G, R) = cv2.split(frame)
                
                R = cv2.merge([zeros, zeros, R])
                G = cv2.merge([zeros, G, zeros])
                B = cv2.merge([B, zeros, zeros])
                 
                # construct the final output frame, storing the original frame
                # at the top-left, the red channel in the top-right, the green
                # channel in the bottom-right, and the blue channel in the
                # bottom-left
                #dibuja tambien videos en BRG
                #output = np.zeros((h * 2, w * 2, 3), dtype="uint8")
                output = np.zeros((h, w, 3), dtype="uint8")
                output[0:h, 0:w] = frame
                #dibuja tambien videos en BRG
                #output[0:h, w:w * 2] = R
                #output[h:h * 2, w:w * 2] = G
                #output[h:h * 2, 0:w] = B
                 
                # write the output frame to file
                writer.write(output)                    
                #cv2.imshow("Output", output)
            
            #muestra el resultado
            if args.show_skeleton==0:
                #sin esqueleto
                resized = cv2.resize(display_image, dim, interpolation = cv2.INTER_AREA)
                cv2.imshow('Link - Control y Bienestar Call Center', resized)
            else:             
                #dibuja el esqueleto
                resized = cv2.resize(overlay_image, dim, interpolation = cv2.INTER_AREA)
                cv2.imshow('Link - Control y Bienestar Call Center', resized)
            
            frame_count += 1
            #si se presiona q se termina el programa
            if cv2.waitKey(1) & 0xFF == ord('q'):
                if args.save_video==1:
                    writer.release()
                break
            
            if args.save_data==1:
                frames_passed += 1
                #imprimo las coordenadas
                if frames_passed==args.save_each:
                    print("======================================================")
                    print(time.ctime())
                    frames_passed = 0
                    if (len(pose_scores)==0):
                        print("==> no se encontro nada")
                    else:
                        print("==> capturando puntos")
                    
                    cuando = time.time()
                    if args.save_image==1:
                        #grabo una captura del video
                        img_name = "/home/pablo/capturas/captura_" + str(cuando) + "_{}.png".format(img_counter)
                        cv2.imwrite(img_name, overlay_image)
                        print("==> {} creado".format(img_name))
                        img_counter += 1
                    else:
                        img_name =""
                        
                    objs = {"date_time":cuando, 
                            "camara": args.camera, 
                            "modelo": args.model,
                            "min_score": args.min_score,
                            "imagen": img_name,
                            "total": 0,
                            "poses":[]}
                    for pi in range(len(pose_scores)):
                        obj = {}
                        if pose_scores[pi] == 0.:
                            #print(str(pi) + " no procesable")
                            break
                        obj["pose_number"] = pi
                        obj["score"] = pose_scores[pi]
                        obj["date_time"] = cuando
                        obj["keypoints"] = {}
                        
                        #print('Pose #%d, score = %f' % (pi, pose_scores[pi]))
                        for ki, (s, c) in enumerate(zip(keypoint_scores[pi, :], keypoint_coords[pi, :, :])):
                            obj["keypoints"][posenet.PART_NAMES[ki]]={"part":posenet.PART_NAMES[ki],"score":s,"coord":c.tolist()}
                            #print('Keypoint %s, score = %f, coord = %s' % (posenet.PART_NAMES[ki], s, c))
                            
                        #print(obj)
                        objs["poses"].append(obj)
                    
                    #print(objs)
                    objs["total"]=len(objs["poses"])
                    print("==> guardo datos")
                    mycol.insert_one(objs)            
            
        print('FPS promedio: ', frame_count / (time.time() - start))

if __name__ == "__main__":
    main()